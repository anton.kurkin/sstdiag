#ifndef DISPLAYWIDGET_H
#define DISPLAYWIDGET_H

#include <QtGui>
#include <QWidget>

#include "transformer.h"
#include "cyclestate.h"
#include "difftrans.h"
#include "austtrans.h"
#include "nodifftrans.h"

class DisplayWidget : public QWidget
{
	Q_OBJECT
public:
	DisplayWidget(QWidget *parent = 0);

	void setTransformation(Transformer* trans);
	void setCycle(QVector<cycleState*>* cycle, bool calccc);
	void setTKD(QVector<QVector<cycleState*>*>* TKD);
	void setFlags(bool logarithmic, bool tbackward, bool tkd, bool itd, bool cc, bool transform, bool dilatom);

	void paint(QPainter &painter);

public slots:

private:
	Transformer* trans;
	QVector <cycleState*>* cycle;
	QVector<QVector<cycleState*>*>* TKD;
	bool logarithmic, tbackward, tkd, itd, cc, transform, dilatom, calccc;

	QPainter painter;

	QPointF calcDisplayPoint(QPointF p);
	void paintLine(QPainter* painter, QLineF line);

	float findCycleMaxTime();
	float findITDMaxTime();
	float findTKDMaxTime();
	void recalcWidth();

	void paintDiffTrans(QPainter &painter, diffTrans* trans);
	void paintAustTrans(QPainter &painter, austTrans* trans);
	void paintnoDiffTrans(QPainter &painter, nodiffTrans* trans);
	void paintAc1Ac3(QPainter &painter, float Ac1, float Ac3);

	float MaxWidth;
	enum {
		MaxHeight = 1450,
		Borders = 10
	} Const ;

protected:
	void paintEvent(QPaintEvent *event);
};

#endif // DISPLAYWIDGET_H
