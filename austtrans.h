#ifndef AUSTTRANS_H
#define AUSTTRANS_H

//class that describes and calculate transformation to Austenite
class austTrans
{
private:
	float c;	//exp(-b/k) (b is shift of incubation period at Tmin temperature)
	double n;	//avraami`s degree, speed of transformation
	float Tmin;	//minimum temperature
	float k;	//steepness of icubation period from temperature

	double nh;	//avraami`s degree, speed of homogenisation

	float calcc(float tau1, float p1, double n, float Ac1, float Tmin, float k);
	double calcn(float tau1, float p1, float tau2, float p2);
	float calcTmin(float Ac1, float pAc1, float Ac3, float pAc3);
	void set(float tau, double n, double nh, float Tmin, float k);

public:
	austTrans();
	austTrans(float tau1, float p1, double n, double nh, float Ac1, float Ac3, float pAc3);
	austTrans(float tau1, float p1, float tau2, float p2, float tauh, float ph, float Ac1, float Ac3);

	inline float getc()		{ return c; }
	inline double getn()	{ return n; }
	inline double getnh()	{ return nh; }
	inline float getTmin()	{ return Tmin; }
	inline float getk()		{ return k; }

	float calctau(float T, float p);
	float calctauh(float T, float p);
	float getb();

	float calcChange(float T, float dt, float current, float possible);
	float calcHomogenization(float T, float dt, float current);
};

#endif // AUSTTRANS_H
