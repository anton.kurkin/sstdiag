#include "phasestateanalizer.h"

//set steels properties coefficients
phaseStateAnalizer::phaseStateAnalizer(float dil_L, float dil_A, float dil_FPB, float dil_M, float dil_sectT)
{
	this->dil_L = dil_L;
	this->dil_A = dil_A;
	this->dil_FPB = dil_FPB;
	this->dil_M = dil_M;
	this->dil_sectT = dil_sectT;
	this->dil_secte = dil_sectT*dil_A;
}

float phaseStateAnalizer::calcStateDilatation(phaseState* phase, float T)
{
	return dil_secte - (dil_sectT-T)*(phase->getL()*dil_L + phase->getA()*dil_A +
			(phase->getF()+phase->getB())*dil_FPB +
			phase->getM()*dil_M);
}

float phaseStateAnalizer::getSecte()
{
	return dil_secte;
}
