
#include <QtGui>
#include <QFileDialog>
#include <QPainter>
#include <QSvgGenerator>
#include <iostream>
#include <exception>

#include "mainwindow.h"
#include "displaywidget.h"
#include "chemicalprops.h"
#include "transformer.h"
#include "cyclestate.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent)
{
	setupUi(this);

	connect(ccTable,SIGNAL(itemClicked(QTableWidgetItem*)), this, SLOT(itemClickedSlot(QTableWidgetItem*)));

	connect(itdTable,SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(itemChangedSlot(QTableWidgetItem*)));
	connect(ccTable,SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(itemChangedSlot(QTableWidgetItem*)));
	connect(structTable,SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(itemChangedSlot(QTableWidgetItem*)));
	connect(chemTable,SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(itemChangedSlot(QTableWidgetItem*)));

	connect(logarithmBox, SIGNAL(stateChanged(int)), this, SLOT(checkboxStateChangedSlot()));
	connect(tkdBox, SIGNAL(stateChanged(int)), this, SLOT(checkboxStateChangedSlot()));
	connect(overtBox, SIGNAL(stateChanged(int)), this, SLOT(checkboxStateChangedSlot()));
	connect(itdBox, SIGNAL(stateChanged(int)), this, SLOT(checkboxStateChangedSlot()));
	connect(transBox, SIGNAL(stateChanged(int)), this, SLOT(checkboxStateChangedSlot()));
	connect(ccBox, SIGNAL(stateChanged(int)), this, SLOT(checkboxStateChangedSlot()));
	connect(dilatomBox, SIGNAL(stateChanged(int)), this, SLOT(checkboxStateChangedSlot()));

	connect(calcButton,SIGNAL(clicked()), this, SLOT(on_calcButton_clicked()));
	connect(calcButton2,SIGNAL(clicked()), this, SLOT(on_calcButton_clicked()));

	sidepanel_button->click();
	checkboxStateChangedSlot();
}

MainWindow::~MainWindow()
{
}

void MainWindow::on_saveSVGButton_clicked()
{
	QString newPath = QFileDialog::getSaveFileName(this, tr("Save Diagram"),
												   path, tr("SVG files (*.svg)"));

	if (newPath.isEmpty())
		return;

	path = newPath;

	QSvgGenerator generator;
	generator.setFileName(path);
	generator.setSize(QSize(displayWidget->width(), displayWidget->height()));
	generator.setViewBox(QRect(0, 0, displayWidget->width(), displayWidget->height()));
	generator.setTitle(tr("SST diagram"));
	generator.setDescription(tr("Isothermic solid state transformation diagram"));

	QPainter painter;
	painter.begin(&generator);
	displayWidget->paint(painter);
	painter.end();
}

void MainWindow::cleanStrToFloat(QString &str)
{
	QString newstr;
	bool isSigned = false;
	bool isFloat = false;
	bool isNullHeaded = false;

	for(int i = 0; i < str.length(); i++)
	{
		if (((!isSigned && 0 == newstr.count()) || (isSigned && 1 == newstr.count())) && '0' == str[i])
		{
			isNullHeaded = true;
			continue;
		}

		if (!isSigned && !isNullHeaded && 0 == newstr.count() && '-' == str[i])
		{
			newstr.push_back('-');
			isSigned = true;
			continue;
		}

		if (!isFloat && ('.' == str[i] || ',' == str[i]) && i != (str.length()-1))
		{
			if ((isSigned && 1 == newstr.count()) || (!isSigned && 0 == newstr.count()))
				newstr.push_back('0');
			newstr.push_back('.');
			isFloat = true;
			continue;
		}

		if (str[i].isDigit())
			newstr.push_back(str[i]);
	}

	if (isSigned && 1 == newstr.count())
		newstr.clear();
	if (!isSigned && 0 == newstr.count())
		newstr.push_back('0');

	str = newstr;
}

void MainWindow::itemFloatCheck(QTableWidgetItem *item)
{
	QString str = item->text();
	cleanStrToFloat(str);
	item->setData(Qt::DisplayRole, str);
}

void MainWindow::insertTableRow(int rown)
{
	ccTable->insertRow(rown+1);
	ccTable->setVerticalHeaderLabels(QStringList(tr("")));
	ccTable->setItem(rown+1,0,new QTableWidgetItem(tr("0")));
	ccTable->setItem(rown+1,1,new QTableWidgetItem(tr("0")));
}

void MainWindow::deleteAllTableRows()
{
	ccTable->clearContents();
	ccTable->setRowCount(0);
}

int MainWindow::deleteTableRow(int rown)
{
	if (1 == ccTable->rowCount())
	{
		insertTableRow(rown);
		ccTable->removeRow(1);
		return 0;
	}

	if (rown == ccTable->rowCount() - 1)
	{
		ccTable->removeRow(rown);
		return rown - 1;
	}

	ccTable->removeRow(rown);
	return rown;
}

int MainWindow::upTableRow(int rown)
{
	if (rown == 0) return 0;

	ccTable->insertRow(rown+1);
	ccTable->setItem(rown+1,0,ccTable->takeItem(rown-1,0));
	ccTable->setItem(rown+1,1,ccTable->takeItem(rown-1,1));
	deleteTableRow(rown-1);
	return rown-1;
}

int MainWindow::downTableRow(int rown)
{
	if (ccTable->rowCount() - 1 == rown) return rown;

	ccTable->insertRow(rown);
	rown += 1;
	ccTable->setItem(rown-1,0,ccTable->takeItem(rown+1,0));
	ccTable->setItem(rown-1,1,ccTable->takeItem(rown+1,1));
	deleteTableRow(rown+1);
	return rown;
}

void MainWindow::checkboxStateChangedSlot()
{
	// paint properties
	bool logarithmic = logarithmBox->isChecked();
	bool tbackward = overtBox->isChecked();
	bool tkd = tkdBox->isChecked();
	bool itd = itdBox->isChecked();
	bool cc = ccBox->isChecked();
	bool transform = transBox->isChecked();
	bool dilatom = dilatomBox->isChecked();
	displayWidget->setFlags(logarithmic, tbackward, tkd, itd, cc, transform, dilatom);
}

void MainWindow::itemChangedSlot(QTableWidgetItem *item)
{
	itemFloatCheck(item);
}

void MainWindow::itemClickedSlot(QTableWidgetItem *item)
{
	table_cell.setX(item->column());
	table_cell.setY(item->row());
}

void MainWindow::on_addButton_clicked()
{
	insertTableRow(table_cell.y());
}

void MainWindow::on_deleteButton_clicked()
{
	table_cell.setY(deleteTableRow(table_cell.y()));
}

void MainWindow::on_upButton_clicked()
{
	table_cell.setY(upTableRow(table_cell.y()));
	repaint();
}

void MainWindow::on_downButton_clicked()
{
	table_cell.setY(downTableRow(table_cell.y()));
	repaint();
}

void MainWindow::on_sidepanel_button_clicked()
{
	if (sidepanel_button->isChecked())
	{
		//Ui_MainWindow::centralWidget->setMinimumSize(1160,520);
		CalcDigitTabWidget->show();
	}
	else
	{
		//Ui_MainWindow::centralWidget->setMinimumSize(500,520);
		CalcDigitTabWidget->hide();
	}
}

void MainWindow::on_saveDataButton_clicked()
{
	QString newPath = QFileDialog::getSaveFileName(this, tr("Save Data"),
												   path, tr("txt files (*.txt)"));

	if (newPath.isEmpty())
		return;

	path = newPath;

	QFile file(path);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	QTextStream out(&file);
	QApplication::setOverrideCursor(Qt::WaitCursor);

	out << deltatEdit->text() << '\n'
		<< TstartEdit->text() << '\n'
		<< '\n';

	for (int i = 0; i < structTable->rowCount(); i++)
		out << structTable->item(i,0)->text() << '\t';
	out << '\n' << '\n';

	for (int i = 0; i < chemTable->rowCount(); i++)
		out << chemTable->item(i,0)->text() << '\t';
	out << '\n' << '\n';

	for (int i = 0; i < itdTable->rowCount(); i++)
	{
		for (int j = 0; j < itdTable->columnCount(); j++)
			out << itdTable->item(i,j)->text() << '\t';
		out << '\n';
	}
	out << '\n';

	for (int i = 0; i < ccTable->rowCount(); i++)
	{
		out << ccTable->item(i,0)->text() << '\t'
			<< ccTable->item(i,1)->text() << '\n';
	}

	QApplication::restoreOverrideCursor();
	file.close();
}

void MainWindow::on_loadDataButton_clicked()
{
	QString newPath = QFileDialog::getOpenFileName(this, tr("Open Data"),
												   path, tr("txt files (*.txt)"));

	if (newPath.isEmpty())
		return;

	path = newPath;

	QFile file(path);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;

	QTextStream in(&file);
	QApplication::setOverrideCursor(Qt::WaitCursor);

	QString line = in.readLine();
	deltatEdit->setText(line);

	line = in.readLine();
	TstartEdit->setText(line);

	line = in.readLine(); // formatting \n

	line = in.readLine();
	for (int i = 0; line.length() && i < structTable->rowCount(); i++)
	{
		structTable->item(i, 0)->setData(Qt::DisplayRole, line.left(line.indexOf('\t')));
		QString tmpline = line.right(line.count() - (line.indexOf('\t') + 1));
		line = tmpline;
	}
	line = in.readLine(); // formatting \n

	line = in.readLine();
	for (int i = 0; line.length() && i < chemTable->rowCount(); i++)
	{
		chemTable->item(i, 0)->setData(Qt::DisplayRole, line.left(line.indexOf('\t')));
		QString tmpline = line.right(line.count() - (line.indexOf('\t') + 1));
		line = tmpline;
	}
	line = in.readLine(); // formatting \n

	for (int i = 0; (line = in.readLine()).length() && i < itdTable->rowCount(); i++)
	{
		for (int j = 0; line.length() && j < itdTable->columnCount(); j++)
		{
			itdTable->item(i, j)->setData(Qt::DisplayRole, line.left(line.indexOf('\t')));
			QString tmpline = line.right(line.count() - (line.indexOf('\t') + 1));
			line = tmpline;
		}
	}

	deleteAllTableRows();
	for (int i = 0; (line = in.readLine()).length(); i++)
	{
		insertTableRow(i-1);
		QString tmp1 = line.left(line.indexOf('\t'));
		QString tmp2 = line.right(line.count() - line.indexOf('\t'));

		ccTable->item(i, 0)->setData(Qt::DisplayRole, line.left(line.indexOf('\t')));
		ccTable->item(i, 1)->setData(Qt::DisplayRole, line.right(line.count() - (line.indexOf('\t') + 1)));
	}

	QApplication::restoreOverrideCursor();
	file.close();
}

void MainWindow::on_nhEdit_editingFinished()
{
	QString str = nhEdit->text();
	cleanStrToFloat(str);
	nhEdit->setText(str);
}

void MainWindow::on_deltatEdit_editingFinished()
{
	QString str = deltatEdit->text();
	cleanStrToFloat(str);
	deltatEdit->setText(str);
}

void MainWindow::on_TstartEdit_editingFinished()
{
	QString str = TstartEdit->text();
	cleanStrToFloat(str);
	TstartEdit->setText(str);
}

void MainWindow::on_CalcParms_clicked()
{
	UChem chem;
	for (int i = 0; i < chemN; i++)
		chem.array[i] = chemTable->item(i,0)->text().toFloat();

	chemicalProps* chemprops = new chemicalProps(&chem);
	if(chemprops->changed())
	{
		const UChem* chem = chemprops->getChem();
		for (int i = 0; i < chemN; i++)
			chemTable->item(i,0)->setText(QString("%1").arg(chem->array[i]));
	}

	Transformer* trans = chemprops->regressProps();

	itdTable->item(Austenite,2)->setText(QString("%1").arg(trans->getAc3()));
	itdTable->item(Austenite,3)->setText(QString("%1").arg(trans->getAc1()));

	itdTable->item(Ferrite,0)->setText(QString("%1").arg(trans->getFerrite()->gettau()));
	itdTable->item(Ferrite,1)->setText(QString("%1").arg(trans->getFerrite()->getn()));
	itdTable->item(Ferrite,2)->setText(QString("%1").arg(trans->getFerrite()->getTmax()));
	itdTable->item(Ferrite,3)->setText(QString("%1").arg(trans->getFerrite()->getTmin()));

	itdTable->item(Bainite,0)->setText(QString("%1").arg(trans->getBainite()->gettau()));
	itdTable->item(Bainite,1)->setText(QString("%1").arg(trans->getBainite()->getn()));
	itdTable->item(Bainite,2)->setText(QString("%1").arg(trans->getBainite()->getTmax()));
	itdTable->item(Bainite,3)->setText(QString("%1").arg(trans->getBainite()->getTmin()));

	itdTable->item(Martensite,1)->setText(QString("%1").arg(trans->getMartensite()->getk()));
	itdTable->item(Martensite,2)->setText(QString("%1").arg(trans->getMartensite()->getTstart()));

	delete trans;

	on_calcButton_clicked();
}

void MainWindow::on_calcButton_clicked()
{
	// -------------------------------------read transformation properties---------------------------------------
	float tau, c, Tmax, Tmin, Ac1, Ac3;
	double n, nh;
	float p1 = 0.01;	//suppose that tau's here are for 5%
	float p99 = 0.99;	//suppose that Ac3 here is for 99%

	//TODO: implement functionality (LIQUID, AUSTENITE AND TEMPERED AUSTENITE are not working)
//	n		= itdTable->item(Liquid,1)->text().toFloat();
//	Tmin	= itdTable->item(Liquid,3)->text().toFloat();
	nodiffTrans* tLiquid = new nodiffTrans();

	c		= itdTable->item(Austenite,0)->text().toFloat();
	n		= itdTable->item(Austenite,1)->text().toFloat();
	Ac3		= itdTable->item(Austenite,2)->text().toFloat();
	Ac1		= itdTable->item(Austenite,3)->text().toFloat();
	nh		= nhEdit->text().toFloat();
	austTrans* tAustenite = new austTrans(c, p1, n, nh, Ac1, Ac3, p99);

	tau		= itdTable->item(Ferrite,0)->text().toFloat();
	n		= itdTable->item(Ferrite,1)->text().toFloat();
	Tmax	= itdTable->item(Ferrite,2)->text().toFloat();
	Tmin	= itdTable->item(Ferrite,3)->text().toFloat();
	diffTrans* tFerrite = new diffTrans(tau, p1, n, Tmax, Tmin);

	tau		= itdTable->item(Bainite,0)->text().toFloat();
	n		= itdTable->item(Bainite,1)->text().toFloat();
	Tmax	= itdTable->item(Bainite,2)->text().toFloat();
	Tmin	= itdTable->item(Bainite,3)->text().toFloat();
	diffTrans* tBainite = new diffTrans(tau, p1, n, Tmax, Tmin);

	n		= itdTable->item(Martensite,1)->text().toFloat();
	Tmax	= itdTable->item(Martensite,2)->text().toFloat();
	nodiffTrans* tMartensite = new nodiffTrans(n, Tmax);

	Transformer* trans = new Transformer(tLiquid, tAustenite, tFerrite, tBainite, tMartensite, Ac1, Ac3);
	displayWidget->setTransformation(trans);

	//----------------------------------------read thermal cycle------------------------------------------------------
	QVector<QPair<float, float>* > thermalCycle;
	for (int i = 0; i < ccTable->rowCount(); i++)
	{
		float stept = ccTable->item(i,0)->text().toFloat();
		float stepw = ccTable->item(i,1)->text().toFloat();
		QPair<float, float>* step = new QPair<float, float> (stept, stepw);
		thermalCycle.append(step);
	}

	float TStart = TstartEdit->text().toFloat();
	float deltat = deltatEdit->text().toFloat();

	//----------------------------------------read start structure------------------------------------------------------
	UPhases phasesStart;
	float phaseTotal = 0;
	float phaseTotalMax = maxPhasePerc;
	for (int i = 0; i < phaseN; i++)
	{
		phasesStart.array[i] = structTable->item(i,0)->text().toFloat();
		if (phasesStart.array[i] < minPhasePerc) phasesStart.array[i] = minPhasePerc;
		else if (phasesStart.array[i] > maxPhasePerc) phasesStart.array[i] = maxPhasePerc;

		phaseTotal += phasesStart.array[i];
		if (phaseTotal > phaseTotalMax)
		{
			phasesStart.array[i] -= phaseTotal - phaseTotalMax;
			phaseTotal = phaseTotalMax;
		}
		structTable->item(i,0)->setText(QString("%1").arg(phasesStart.array[i], 0, 'f'));
		phaseTotalMax += minPhasePerc;
	}
	if (phaseTotal < 1) phasesStart.array[phaseN-1] += 1 -phaseTotal;
	structTable->item(phaseN-1,0)->setText(QString("%1").arg(phasesStart.array[phaseN-1], 0, 'f'));

	QVector<cycleState*>* cycle = trans->calcCycle(&thermalCycle, TStart, deltat, &phasesStart);

	displayWidget->setCycle(cycle, true);

	//----------TKD------------
	UPhases phasesStartTKD;
	for (int i = 0; i < phaseN; i++)
		phasesStartTKD.array[i] = minPhasePerc;
	phasesStartTKD.A = maxPhasePerc;

	QVector<QVector<cycleState*>*>* TKD = trans->calcTKD(0.01, 10000., 1.1, false, 780., 0., 1, &phasesStartTKD);
	displayWidget->setTKD(TKD);

	//debug output
//	while (cycle->size()) {
//		cycleState* tmp = *cycle->begin();
//		cycle->pop_front();
//		printf("T%f t%f: L%f, A%f, F%f, P%f, B%f, M%f, TM%f\n", tmp->getT(), tmp->gett(), tmp->getphases()->getL(), tmp->getphases()->getA(), tmp->getphases()->getF(), tmp->getphases()->getP(), tmp->getphases()->getB(), tmp->getphases()->getM(), tmp->getphases()->getTM());
//	}
}

