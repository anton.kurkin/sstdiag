#include <QtGui>
#include <QtAlgorithms>
#include <QVectorIterator>
#include "displaywidget.h"
#include "mainwindow.h"
#include "phasestate.h"
#include "phasestateanalizer.h"

const float p99 = 0.99; // searching for tau at p = 99% - "end" of diffusion transformations
const float p90 = 0.90; // searching for tau at p = 90% - "end" of no diffusion transformations

DisplayWidget::DisplayWidget(QWidget *parent)
	: QWidget(parent)
{
	logarithmic = itd = cc = transform = dilatom = calccc = false;
	setBackgroundRole(QPalette::Base);
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	cycle = NULL;
	TKD = NULL;
	trans = NULL;
}

void DisplayWidget::paintEvent(QPaintEvent *)
{
	painter.begin(this);
	paint(painter);
	painter.end();
}

void DisplayWidget::setTransformation(Transformer *trans)
{
	delete this->trans;
	this->trans = trans;

	recalcWidth();
	update();
}

void DisplayWidget::setCycle(QVector<cycleState *> *cycle, bool calccc)
{
	if(NULL != this->cycle)
	{
		if (this->cycle->size() > 0)
		{
			qDeleteAll(*(this->cycle));
			this->cycle->clear();
		}
		delete this->cycle;
	}
	this->cycle = cycle;
	this->calccc = calccc;

	recalcWidth();
	update();
}

void DisplayWidget::setTKD(QVector<QVector<cycleState*>*>* TKD)
{
	if(NULL != this->TKD)
	{
		if (this->TKD->size() > 0)
		{
			QVectorIterator<QVector<cycleState*>*> i(*(this->TKD));
			while (i.hasNext())
			{
				QVector<cycleState *> *cycle = i.next();
				if (cycle->size() > 0) {
					qDeleteAll(*cycle);
					cycle->clear();
				}
			}
			qDeleteAll(*(this->TKD));
			this->TKD->clear();
		}
		delete this->TKD;
	}
	this->TKD = TKD;

	recalcWidth();
	update();
}

void DisplayWidget::setFlags(bool logarithmic, bool tbackward, bool tkd, bool itd, bool cc, bool transform, bool dilatom)
{
	this->logarithmic = logarithmic;
	this->tbackward = tbackward;
	this->tkd = tkd;
	this->itd = itd;
	this->cc = cc;
	this->transform = transform;
	this->dilatom = dilatom;

	recalcWidth();
	update();
}

float DisplayWidget::findCycleMaxTime()
{
	if (NULL == cycle) return 0;

	return cycle->last()->gett();
}

float DisplayWidget::findITDMaxTime()
{
	if (NULL == trans) return 0;

	float t = trans->getAustenite()->calctau(trans->getAc1(), p99);
	t = std::max(t, trans->getFerrite()->calctau(p99));
	t = std::max(t, trans->getBainite()->calctau(p99));
	return t;
}

float DisplayWidget::findTKDMaxTime()
{
	if (NULL == this->TKD) return 0;

	float t = 0;
	QVectorIterator<QVector<cycleState*>*> i(*(this->TKD));
	while (i.hasNext())
	{
		QVector<cycleState *> *cycle = i.next();
		QVectorIterator<cycleState*> j(*cycle);
		while (j.hasNext())
		{
			cycleState *state = j.next();
			t = std::max(t, state->gett());
		}
	}
	return t;
}

void DisplayWidget::recalcWidth()
{
	// maximum width(t) calculation
	MaxWidth = 0;
	if (itd & !(transform || cc))
		MaxWidth = std::max(MaxWidth, findITDMaxTime());
	if (transform || cc)
		MaxWidth = std::max(MaxWidth, findCycleMaxTime());
	if (tkd)
		MaxWidth = std::max(MaxWidth, findTKDMaxTime());
}

QPointF DisplayWidget::calcDisplayPoint(QPointF p)
{
	if (MaxWidth <= 0 || MaxHeight <= 0) return QPointF(0, 0);
	if (logarithmic && (p.x() <= 0 || MaxWidth <= 1)) return QPointF(0, 0);
	int curwidth = this->width() - 2 * Borders;
	int curheight = this->height() - 2 * Borders;

	float x, y;
	y = -(p.y() / MaxHeight) * curheight;

	if (!logarithmic)
		x = (p.x() / MaxWidth) * curwidth;
	else
		x = (log(p.x()) / log(MaxWidth)) * curwidth;

	return QPointF(x, y);
}

void DisplayWidget::paintLine(QPainter* painter, QLineF line)
{
	painter->drawLine(calcDisplayPoint(line.p1()),calcDisplayPoint(line.p2()));
}

const QColor colors[phaseN] = {QColor(100, 100, 100, 255), QColor(255, 0, 0, 255), QColor(0,255,0, 255), QColor(255, 80, 255, 255), QColor(0, 0, 255, 255)};

void DisplayWidget::paintDiffTrans(QPainter &painter, diffTrans *trans)
{
	paintLine(&painter, QLineF(trans->gettau(),		trans->getTmin(), trans->gettau(),		trans->getTmax()));
	paintLine(&painter, QLineF(trans->gettau(),		trans->getTmax(), trans->calctau(p99),	trans->getTmax()));
	paintLine(&painter, QLineF(trans->calctau(p99),	trans->getTmax(), trans->calctau(p99),	trans->getTmin()));
	paintLine(&painter, QLineF(trans->calctau(p99),	trans->getTmin(), trans->gettau(),		trans->getTmin()));
}

void DisplayWidget::paintAustTrans(QPainter &painter, austTrans *trans)
{
	int oldT = trans->getTmin()+0.001;
	float oldtau1 = trans->calctau(oldT, 0.01);
	float oldtau99 = trans->calctau(oldT, 0.99);
	for (float T = trans->getTmin()+0.01; T < 1350; T+=10) {
		float tau1 = trans->calctau(T, 0.01);
		float tau99 = trans->calctau(T, 0.99);
		paintLine(&painter, QLineF(tau1,	T, oldtau1,		oldT));
		paintLine(&painter, QLineF(tau99,	T, oldtau99,	oldT));
		oldT = T;
		oldtau1 = tau1;
		oldtau99 = tau99;
	}
}

void DisplayWidget::paintnoDiffTrans(QPainter &painter, nodiffTrans *trans)
{
	paintLine(&painter, QLineF(0, trans->getTstart(), MaxWidth, trans->getTstart()));
	float Tend = trans->calcT(p90);
	paintLine(&painter, QLineF(0, Tend, MaxWidth, Tend));
}

void DisplayWidget::paintAc1Ac3(QPainter &painter, float Ac1, float Ac3)
{
	paintLine(&painter, QLineF(0, Ac1, MaxWidth, Ac1));
	paintLine(&painter, QLineF(0, Ac3, MaxWidth, Ac3));
}

void DisplayWidget::paint(QPainter &painter)
{
	// cleaning
	painter.fillRect(QRect(0, 0, width(), height()), Qt::white);
	painter.translate(Borders, this->height() - Borders);

	// ----------------------------------------------------------------------------------------
	// grid on graph
	int curheight = this->height() - 2 * Borders;
	int curwidth = this->width() - 2 * Borders;

	painter.setPen(QPen(Qt::black, 1));
	painter.drawLine(0,0,0,-(curheight - Borders));
	for (int i = 0; i < MaxHeight; i += 100)
		painter.drawLine(0,-(curheight*i/MaxHeight),curwidth,-(curheight*i/MaxHeight));

	if (!logarithmic)
	{
		int inc = 1;
		double widthTmp = MaxWidth;
		while ((widthTmp /= 10) > 3)
			inc *= 10;
		for (float i = 0; i < MaxWidth; i += inc)
			painter.drawLine(curwidth*i/MaxWidth,0,curwidth*i/MaxWidth,-curheight);
	}
	else
		for (float i = 1; i < MaxWidth; i *= 10)
		{
			for (float j = i; j < i*10; j += i)
				painter.drawLine((log(j) / log(MaxWidth)) * curwidth,0,(log(j) / log(MaxWidth)) * curwidth,-curheight);
		}

	if (NULL == trans) return;

	// ----------------------------------------------------------------------------------------
	// isotermic diagram
	if (itd)
	{
		const int linewidth = 3;
		painter.setPen(QPen(colors[Liquid], linewidth));
		paintnoDiffTrans(painter, trans->getLiquid());

		painter.setPen(QPen(colors[Austenite], linewidth));
		paintAustTrans(painter, trans->getAustenite());
		paintAc1Ac3(painter, trans->getAc1(), trans->getAc3());

		painter.setPen(QPen(colors[Ferrite], linewidth));
		paintDiffTrans(painter, trans->getFerrite());

		painter.setPen(QPen(colors[Bainite], linewidth));
		paintDiffTrans(painter, trans->getBainite());

		painter.setPen(QPen(colors[Martensite], linewidth));
		paintnoDiffTrans(painter, trans->getMartensite());
	}

	if (cycle != NULL && cycle->size() >= 2)
	{
		// ----------------------------------------------------------------------------------------
		// cooling cycle
		if (cc)
		{
			const int linewidth = 3;
			for (int i = 1; i < cycle->size(); i++)
			{
				painter.setPen(QPen(Qt::black, linewidth));
				paintLine(&painter, QLineF((*cycle)[i-1]->gett(), (*cycle)[i-1]->getT(),
										(*cycle)[i]->gett(), (*cycle)[i]->getT()));
			}
		}

		if (false == calccc) return;

		// ----------------------------------------------------------------------------------------
		// transformations
		if (transform)
		{
			const int linewidth = 3;
			/*
			//old phases display way
			phaseState* prev = (*cycle)[0]->getphases();
			float prevt = (*cycle)[0]->gett();
			for (int i = 1; i < cycle->size(); i++)
			{
				painter.setPen(QPen(colors[Liquid], linewidth));
				paintLine(&painter, QLineF(prevt, prev->getL()*MaxHeight,
										currt, curr->getL()*MaxHeight));

				painter.setPen(QPen(colors[Austenite], linewidth));
				paintLine(&painter, QLineF(prevt, prev->getA()*MaxHeight,
										currt, curr->getA()*MaxHeight));

				painter.setPen(QPen(colors[Ferrite], linewidth));
				paintLine(&painter, QLineF(prevt, prev->getF()*MaxHeight,
										currt, curr->getF()*MaxHeight));

				painter.setPen(QPen(colors[Bainite], linewidth));
				paintLine(&painter, QLineF(prevt, prev->getB()*MaxHeight,
										currt, curr->getB()*MaxHeight));

				painter.setPen(QPen(colors[Martensite], linewidth));
				paintLine(&painter, QLineF(prevt, prev->getM()*MaxHeight,
										currt, curr->getM()*MaxHeight));
			}
			*/
			//new phases display way
			for (int i = 0; i < cycle->size(); i++)
			{
				phaseState* curr = (*cycle)[i]->getphases();
				float currt = (*cycle)[i]->gett();
				float allprev = 0;

				painter.setPen(QPen(colors[Martensite], linewidth));
				paintLine(&painter, QLineF(currt, allprev*MaxHeight,
										currt, (allprev+curr->getM())*MaxHeight));
				allprev += curr->getM();

				painter.setPen(QPen(colors[Bainite], linewidth));
				paintLine(&painter, QLineF(currt, allprev*MaxHeight,
										currt, (allprev+curr->getB())*MaxHeight));
				allprev += curr->getB();

				painter.setPen(QPen(colors[Ferrite], linewidth));
				paintLine(&painter, QLineF(currt, allprev*MaxHeight,
										currt, (allprev+curr->getF())*MaxHeight));
				allprev += curr->getF();

				painter.setPen(QPen(colors[Austenite], linewidth));
				paintLine(&painter, QLineF(currt, allprev*MaxHeight,
										currt, (allprev+curr->getA())*MaxHeight));
				allprev += curr->getA();

				painter.setPen(QPen(colors[Liquid], linewidth));
				paintLine(&painter, QLineF(currt, allprev*MaxHeight,
										currt, (allprev+curr->getL())*MaxHeight));
				allprev += curr->getL();
			}

			// grid on graph

			painter.setPen(QPen(Qt::black, 1));
			for (int i = 0; i <= 100; i += 10)
				painter.drawLine(0,-(curheight*i/100),curwidth,-(curheight*i/100));

			int inc = 1;
			double widthTmp = MaxWidth;
			while ((widthTmp /= 10) > 3)
				inc *= 10;
			for (float i = 0; i < MaxWidth; i += inc)
				painter.drawLine(curwidth*i/MaxWidth,0,curwidth*i/MaxWidth,-curheight);
		}

		// ----------------------------------------------------------------------------------------
		// dilatogramm
		if (dilatom)
		{
			const int linewidth = 5;
			painter.setPen(QPen(Qt::black, linewidth));
			phaseStateAnalizer* analizer = new phaseStateAnalizer(0, 16, 12, 8, 1270);
			float paintk = MaxWidth/analizer->getSecte();

			float prev = analizer->calcStateDilatation((*cycle)[0]->getphases(), (*cycle)[0]->getT());
			float prevT = (*cycle)[0]->getT();
			for (int i = 1; i < cycle->size(); i++)
			{
				float curr = analizer->calcStateDilatation((*cycle)[i]->getphases(), (*cycle)[i]->getT());
				float currT = (*cycle)[i]->getT();
				paintLine(&painter, QLineF(prev*paintk, prevT, curr*paintk, currT));
				prev = curr;
				prevT = currT;
			}
			delete analizer;
		}
	}

	//----------------------------------------------------------------------------------------------
	//TKD
	if (tkd)
	{
		if (TKD != NULL && TKD->size() != 0)
		{
			const int linewidth = 3;

			const int n = 5;
			const float p[n] = {0.01, 0.25, 0.5, 0.75, 0.99};

			float prevT[phaseN][n];
			float prevt[phaseN][n];
			bool isprev[phaseN][n];
			for (int k = 0; k < phaseN; k++)
				for (int l = 0; l < n; l++)
					isprev[k][l] = false;

			QVectorIterator<QVector<cycleState*>*> i(*(this->TKD));

			if (i.hasNext())
			{
				QVector<cycleState *> *cycle = i.next();
				QVectorIterator<cycleState*> j(*cycle);

				bool iscurr[phaseN][n];
				for (int k = 0; k < phaseN; k++)
					for (int l = 0; l < n; l++)
						iscurr[k][l] = false;
				while (j.hasNext())
				{
					cycleState *cstate = j.next();
					phaseState* pstate = cstate->getphases();
					float currT = cstate->getT();
					float currt = cstate->gett();
					for (int k = 0; k < phaseN; k++)
					{
						for (int l = 0; l < n; l++)
						{
							if (!iscurr[k][l] && pstate->get(k) >= p[l])
							{
								prevT[k][l] = currT;
								prevt[k][l] = currt;
								isprev[k][l] = true;
								iscurr[k][l] = true;
							}
						}
					}
				}
			}

			while (i.hasNext())
			{
				QVector<cycleState *> *cycle = i.next();
				QVectorIterator<cycleState*> j(*cycle);

				bool iscurr[phaseN][n];
				for (int k = 0; k < phaseN; k++)
					for (int l = 0; l < n; l++)
						iscurr[k][l] = false;
				while (j.hasNext())
				{
					cycleState *cstate = j.next();
					phaseState* pstate = cstate->getphases();
					float currT = cstate->getT();
					float currt = cstate->gett();
					for (int k = 0; k < phaseN; k++)
					{
						for (int l = 0; l < n; l++)
						{
							if (!iscurr[k][l] && pstate->get(k) >= p[l])
							{
								if (isprev[k][l])
								{
									painter.setPen(QPen(colors[k], linewidth));
									paintLine(&painter, QLineF(prevt[k][l], prevT[k][l], currt, currT));
								}
								prevT[k][l] = currT;
								prevt[k][l] = currt;
								isprev[k][l] = true;
								iscurr[k][l] = true;
							}
						}
					}
				}
			}
		}
	}
}
