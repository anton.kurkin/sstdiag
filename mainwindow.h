#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include "ui_mainwindow.h"

class MainWindow : public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void itemChangedSlot(QTableWidgetItem* item);
	void itemClickedSlot(QTableWidgetItem* item);
	void checkboxStateChangedSlot();
	void on_saveSVGButton_clicked();

	void on_upButton_clicked();
	void on_downButton_clicked();
	void on_addButton_clicked();
	void on_deleteButton_clicked();
	void on_sidepanel_button_clicked();
	void on_saveDataButton_clicked();
	void on_loadDataButton_clicked();

	void on_nhEdit_editingFinished();
	void on_deltatEdit_editingFinished();
	void on_TstartEdit_editingFinished();

	void on_CalcParms_clicked();
	void on_calcButton_clicked();


private:
	void cleanStrToFloat(QString &str);
	void itemFloatCheck(QTableWidgetItem* item);
	void insertTableRow(int rown = -1);
	void deleteAllTableRows();
	int deleteTableRow(int rown = 0);
	int upTableRow(int rown = 0);
	int downTableRow(int rown = 0);
	QString path;

public:
	QPoint table_cell;
};

#endif // MAINWINDOW_H
