#include "austenitetransformation.h"

AusteniteTransformation::AusteniteTransformation (std::vector <double> parameters) : AvramiTransformation(parameters)
{
}

AusteniteTransformation::~AusteniteTransformation()
{
}

phaseState AusteniteTransformation::step (double T, double dt, phaseState oldState, phaseState newState)
{
	double p = calcPhaseFraction ((double)T, (double)dt, (double)oldState.getA(), 1.);

	newState.set (Austenite, p);
	return newState;
}

double AusteniteTransformation::halfLifeTime (double T)
{
	//TODO: create correct model
	return T;
}
