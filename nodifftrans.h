#ifndef NODIFFPROPS_H
#define NODIFFPROPS_H

//class that describes and calculate non-diffusion transformation
class nodiffTrans
{
private:
	double k;	//coefficient of how big temperature change is needed for phase`s transformation
	float Tstart;	//temperature of transformation`s start (lower or upper bound)
	void set(double k, float Tstart);

public:
	nodiffTrans();
	nodiffTrans(double k, float Tstart);
	nodiffTrans(float T1, float p1, float T2, float p2);

	inline double getk()		{ return k; }
	inline double getTstart()	{ return Tstart; }

	float calcT(float p);

	float calcChange(float T, float current, float possible);
};

#endif // NODIFFPROPS_H
