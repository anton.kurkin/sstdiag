#include "avramitransformation.h"
#include <cmath>

AvramiTransformation::AvramiTransformation(std::vector <double> parameters) : Transformation(parameters)
{
}

AvramiTransformation::~AvramiTransformation()
{
}

double AvramiTransformation::percentByTime (double t, double T)
{
	double p = 1 - exp (pow (t / halfLifeTime (T), _n) * log (1 - HALFLIFE_FRACTION));
	return p;
}

double AvramiTransformation::timeByPercent (double p, double T)
{
	double t = halfLifeTime (T) * pow (log (1 - p) / log (1 - HALFLIFE_FRACTION), 1/_n);
	return t;
}

double AvramiTransformation::calcPhaseFraction (double T, double dt, double current, double possible)
{
	if (current >= possible) return current;
	double p = current / possible;
	double t = timeByPercent (p, T) + dt;
	p = percentByTime (t, T);
	return p * possible;
}
