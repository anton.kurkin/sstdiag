#include "nodifftrans.h"
#include <cmath>
#include "phasestate.h"

const float Pgamma = 1; //max %

nodiffTrans::nodiffTrans()
{
	set(0, 0);
}

nodiffTrans::nodiffTrans(double k, float Tstart)
{
	set(k, Tstart);
}

nodiffTrans::nodiffTrans(float T1, float p1, float T2, float p2)
{
	if (T1 == T2 || p1 == p2) { set(0, 0); return; }
	Tstart = (T2*log(1-p1/Pgamma) - T1*log(1-p2/Pgamma))/(log(1-p1/Pgamma) - log(1-p2/Pgamma));
        k  = log(1-p1/Pgamma)/(Tstart - T1);
}

void nodiffTrans::set(double k, float Tstart)
{
	this->k = k;
	this->Tstart = Tstart;
}

//calculate temperature needed for phase percent
float nodiffTrans::calcT(float p)
{	//TODO: check integrity
	if (0 == k) return -274;
	return Tstart + log(1-p)/k;
}

float nodiffTrans::calcChange(float T, float current, float possible)
{
	if (possible <= minPhasePerc) return 0;
	possible -= minPhasePerc;

	float Pgamma = possible + current;
        float change = Pgamma*(1-exp(k * (Tstart - T))) - current;

	return change;
}
