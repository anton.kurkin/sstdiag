#include "transformer.h"
#include "phasestate.h"
#include <algorithm>
#include <cmath>
#include <assert.h>

Transformer::Transformer(nodiffTrans *Liquid, austTrans *Austenite, diffTrans *Ferrite,
						diffTrans *Bainite, nodiffTrans *Martensite, float Ac1, float Ac3)
{
	this->Liquid = Liquid;
	this->Austenite = Austenite;
	this->Ferrite = Ferrite;
	this->Bainite = Bainite;
	this->Martensite = Martensite;
	this->Ac1 = Ac1;
	this->Ac3 = Ac3;
}

Transformer::~Transformer()
{
	delete Liquid;
	delete Austenite;
	delete Ferrite;
	delete Bainite;
	delete Martensite;
}

//calc new structure state
phaseState* Transformer::calcState(phaseState* current, float T, float dt)
{
	if (NULL == current || dt <= 0) return NULL;


	UPhases newState, tmp;

	//set new state as prev step values
	newState.L = current->getL();
	newState.A = current->getA();
	newState.F = current->getF();
	newState.B = current->getB();
	newState.M = current->getM();
	float H = 1;

	//calc balance of FP and A
	float pAc1 = 0.01, pAc3 = 0.99;
	float Aequilibrium = std::max(minPhasePerc,std::min((T*(pAc3-pAc1)+pAc1*Ac3-pAc3*Ac1)/(Ac3-Ac1), maxPhasePerc));

	float phasesToAustenite = current->getF() - minPhasePerc + current->getB() - minPhasePerc + current->getM() - minPhasePerc;
	float possibleAustenite = std::min(phasesToAustenite, Aequilibrium-current->getA());

	//Austenite transformation
	tmp.A = Austenite->calcChange(T,dt,current->getA(),possibleAustenite);
	if (tmp.A > 0)
	{
		H *= newState.A / (newState.A + tmp.A);
		float mod = tmp.A / phasesToAustenite;

		float excess = 0;

		if (newState.F - mod*current->getF() - excess < minPhasePerc) {
			excess = mod*current->getF() + minPhasePerc + excess - newState.F;
			newState.F = minPhasePerc;
		} else {
			newState.F -= mod*current->getF() + excess;
			excess = 0;
		}

		if (newState.B - mod*current->getB() - excess < minPhasePerc) {
			excess = mod*current->getB() + minPhasePerc + excess - newState.B;
			newState.B = minPhasePerc;
		} else {
			newState.B -= mod*current->getB() + excess;
			excess = 0;
		}

		if (newState.M - mod*current->getM() - excess < minPhasePerc) {
			excess = mod*current->getM() + minPhasePerc + excess - newState.M;
			newState.M = minPhasePerc;
		} else {
			newState.M -= mod*current->getM() + excess;
			excess = 0;
		}

		newState.A += tmp.A - excess;
	}
	H += Austenite->calcHomogenization(T,dt,H);

	//ferrite transformation
	float possibleFerrite = std::min(current->getA() - minPhasePerc, 1 - Aequilibrium-current->getF());
	tmp.F = Ferrite->calcChange(T,dt,current->getF(),possibleFerrite,H);
	tmp.F = std::min(newState.A - minPhasePerc, tmp.F);
	newState.A -= tmp.F;
	newState.F += tmp.F;

	//bainite transformation
	tmp.B = Bainite->calcChange(T,dt,current->getB(),newState.A,H);
	tmp.B = std::min(newState.A - minPhasePerc, tmp.B);
	newState.A -= tmp.B;
	newState.B += tmp.B;

	//martensite transformation
	tmp.M = Martensite->calcChange(T,current->getM(),newState.A);
	tmp.M = std::min(newState.A - minPhasePerc, tmp.M);
	if (tmp.M >= 0)
	{
		newState.A -= tmp.M;
		newState.M += tmp.M;
	}
//	else
//	{
//	}

/*	float upk = exp(0.01073f*T)*dt/9.E4;

	//tempering
	if (T > Bainite->getTmin())
	{
		float dM = std::min(maxPhasePerc, upk*newState.M);
		if (dM > newState.M - minPhasePerc)
			dM = newState.M - minPhasePerc;
		newState.M -= dM;
		newState.B += dM;
		if (T > Ferrite->getTmin())
		{
			float dB = std::min(maxPhasePerc, upk*newState.B);
			if (dB > newState.B - minPhasePerc)
				dB = newState.B - minPhasePerc;
			newState.B -= dB;
			newState.F += dB;
		}
	}
*/

	//normalize
	float total = 0;
	for (int i = 0; i < phaseN; i++)
		total += newState.array[i];
	assert (total != 0); // something went terribly wrong if total == 0

	//fix all out of limits (min/maxPhasePerc)
	float excess = 0;
	for (int i = 0; i < phaseN; i++) {
		newState.array[i] /= total;
		if (newState.array[i] > maxPhasePerc) {
			excess += newState.array[i] - maxPhasePerc;
			newState.array[i] = maxPhasePerc;
		}
		else if (newState.array[i] < minPhasePerc) {
			excess -= minPhasePerc - newState.array[i];
			newState.array[i] = minPhasePerc;
		}
	}
	if (excess > 0) {
		float change = 0;
		for (int i = 0; i < phaseN; i++)
			if (newState.array[i] < maxPhasePerc)
				change += newState.array[i];
		float mod = excess/change;
		for (int i = 0; i < phaseN; i++)
			if (newState.array[i] < maxPhasePerc)
				newState.array[i] += mod*newState.array[i];
	} else if (excess < 0) {
		float change = 0;
		for (int i = 0; i < phaseN; i++)
			if (newState.array[i] > minPhasePerc)
				change += newState.array[i];
		float mod = excess/change;
		for (int i = 0; i < phaseN; i++)
			if (newState.array[i] > minPhasePerc)
				newState.array[i] += mod*newState.array[i];
	}

	return new phaseState(&newState);
}

// loop by transformation cycle`s steps with vector of states as output
QVector<cycleState*>* Transformer::calcCycle(QVector<QPair<float, float>* >* thermalCycle, float TStart, float deltat, UPhases* phasesStart)
{
	QVector<cycleState*>* cycle = new QVector<cycleState*>();
	if ((NULL == thermalCycle) || (thermalCycle->isEmpty()) || (deltat <= 0) || (NULL == phasesStart)) return cycle;

	phaseState* stateStart = new phaseState(phasesStart);
	cycleState* cyclestate = new cycleState(TStart, 0, stateStart);
	cycle->append(cyclestate);
	float T = TStart;
	float t = 0;

	float stept = 0;
	const int stepN = thermalCycle->size();
	for (int i = 0; i < stepN; i++)
	{
		stept = stept + (*thermalCycle)[i]->first;
		float stepw = (*thermalCycle)[i]->second;

		for (t+=deltat; t < stept; t+=deltat)
		{
			T -= stepw*deltat;
			phaseState* newPhase = calcState (cycle->last()->getphases(), T, deltat);
			if (NULL == newPhase) return cycle;
			cyclestate = new cycleState(T, t, newPhase);
			cycle->append(cyclestate);
		}
		float enddt = deltat - (t - stept);
		if (enddt > 0)
		{
			t = stept;
			T -= stepw*enddt;
			phaseState* newPhase = calcState (cycle->last()->getphases(), T, enddt);
			if (NULL == newPhase) return cycle;
			cyclestate = new cycleState(T, t, newPhase);
			cycle->append(cyclestate);
		}
	}
	return cycle;
}

// loop by cooling speeds and cycle`s steps with vector of states as output
QVector<QVector<cycleState*>*>* Transformer::calcTKD(float wstart, float wend, float pw, bool w65, float Tstart, float Tend, float deltaT, UPhases* phasesStart)
{
	QVector<QVector<cycleState*>*>* TKD = new QVector<QVector<cycleState*>*>();
	if ((Tstart < Tend) || (wstart <= 0) || (wend <= 0) || (wstart > wend) || (pw <= 1) || (deltaT <= 0) || (NULL == phasesStart)) return TKD;

	float w = wstart;
	while (w <= wend)
	{
		phaseState* stateStart = new phaseState(phasesStart);
		QVector<cycleState*>* cycle = new QVector<cycleState*>();
		cycleState* cyclestate = new cycleState(Tstart, 0, stateStart);
		cycle->append(cyclestate);
		float t = 0;

		float deltaT = 1;
		float deltat = deltaT/w;
		for (float T = Tstart; T >= Tend; T -= deltaT)
		{
			phaseState* newPhase = calcState (cycle->last()->getphases(), T, deltat);
			if (NULL == newPhase) {
				TKD->append(cycle);
				return TKD;
			}
			cyclestate = new cycleState(T, t, newPhase);
			cycle->append(cyclestate);
			t+=deltat;
		}

		TKD->append(cycle);
		w *= pw;
	}
	return TKD;
}
