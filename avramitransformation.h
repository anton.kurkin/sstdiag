#ifndef AVRAMITRANSFORMATION_H
#define AVRAMITRANSFORMATION_H

#include "transformation.h"
#include <vector>

//class that describes and calculate avraami-based transformation
class AvramiTransformation : public Transformation
{
private:
	static constexpr double HALFLIFE_FRACTION = 0.5;
	double _n;

protected:
	virtual double halfLifeTime (double T) = 0;
	double percentByTime (double t, double T);
	double timeByPercent (double p, double T);
	double calcPhaseFraction (double T, double dt, double current, double possible);

public:
	AvramiTransformation (std::vector <double> parameters);
	~AvramiTransformation ();
	virtual phaseState step (double T, double dt, phaseState oldState, phaseState newState) = 0;
};

#endif // AVRAMITRANSFORMATION_H
