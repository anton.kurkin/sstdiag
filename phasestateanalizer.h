#ifndef PHASESTATEANALIZER_H
#define PHASESTATEANALIZER_H

#include "phasestate.h"

//class used for analyze element`s properties by its phase structure
class phaseStateAnalizer
{
private:
	float dil_L, dil_A, dil_FPB, dil_M, dil_sectT, dil_secte; //dilatometric coefficients of phases

public:
	phaseStateAnalizer(float dil_L, float dil_A, float dil_FPBTM, float dil_M, float dil_sectT);
	float calcStateDilatation(phaseState* phase, float T);
	float getSecte();
	float calcStateEnergy(phaseState* phase);
};

#endif // PHASESTATEANALIZER_H
