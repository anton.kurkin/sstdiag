#ifndef CYCLESTATE_H
#define CYCLESTATE_H

#include "phasestate.h"

class cycleState
{
private:
	float T;	//Temperature
	float t;	//time
	phaseState* phases;	//phase structure at this step

public:
	cycleState(float T, float t, phaseState* phases);
	~cycleState();
	inline float getT() { return T; }
	inline float gett() { return t; }
	inline phaseState* getphases() { return phases; }
};

#endif // CYCLESTATE_H
