#ifndef PHASESTATE_H
#define PHASESTATE_H

enum phaseEnum
{
	Liquid, Austenite, Ferrite, Bainite, Martensite, phaseN
};

union UPhases
{
	struct
	{	//Liquid, Austenite, Ferrite, Bainite, Martensite
		float L, A, F, B, M;
	};
	float array[phaseN];
};

const float minPhasePerc = 0.00000001;
const float maxPhasePerc = 1.0 - minPhasePerc*(phaseN-1); // number of phases without proceeded phase

// class for phase structure state at every step
class phaseState
{
public:
private:
	UPhases phases;
public:
	phaseState(UPhases* phases);
	~phaseState();

	inline float getL() {return phases.L;}
	inline float getA() {return phases.A;}
	inline float getF() {return phases.F;}
	inline float getB() {return phases.B;}
	inline float getM() {return phases.M;}
	inline float get(int i) {return phases.array[i];}
	inline void set(int i, float value) {phases.array[i] = value;}
};

#endif // PHASESTATE_H
