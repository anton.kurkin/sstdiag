#include "difftrans.h"
#include <cmath>
#include <algorithm>
#include "phasestate.h"

const float taup = 0.50; //tau - time at p = 50%

diffTrans::diffTrans()
{
	set(0, 0, 0, 0);
}

diffTrans::diffTrans(float tau1, float p1, float tau2, float p2, float Tmax, float Tmin)
{
	double n = calcn(tau1, p1, tau2, p2);
	set(calctau(tau1, p1, n), n, Tmax, Tmin);
}

diffTrans::diffTrans(float tau1, float p1, double n, float Tmax, float Tmin)
{
	set(calctau(tau1, p1, n), n, Tmax, Tmin);
}

//calculate avraami`s formula degree by 2 arbitraty incubation periods
double diffTrans::calcn(float tau1, float p1, float tau2, float p2)
{
	if (p1 == p2 || p1 <= 0 || p1 >= 1 ||
					p2 <= 0 || p2 >= 1 ||
		tau1 == tau2 || tau1 <= 0 || tau2 <= 0)
		return -1;
	return log(log(1-p2)/log(1-p1))/log(tau2/tau1);
}

//calculate proper incubation period (1%) by arbitrary incubation period and avraami`s formula degree
float diffTrans::calctau(float tau1, float p1, double n)
{
	if (n <= 0 || p1 <= 0 || p1 >= 1 || tau1 <= 0) return -1;
	if (p1 == taup) return tau1;

	return tau1*exp(log(log(1-taup)/log(1-p1))/n);
}

//calculate incubation period that`s needed to obtain p % of phase
float diffTrans::calctau(float p)
{
	if (n <= 0 || p <= 0 || p >= 1 || tau <= 0) return -1;
	if (p == taup) return tau;

	return tau*exp(log(log(1-p)/log(1-taup))/n);
}

void diffTrans::set(float tau, double n, float Tmax, float Tmin)
{
	this->tau = tau;
	this->n = n;
	this->Tmax = Tmax;
	this->Tmin = Tmin;
}

float diffTrans::calcChange(float T, float dt, float current, float possible, float homogen)
{
	if (T < Tmin || T > Tmax || n<=0 || tau==0) return 0;

	if (possible <= minPhasePerc || current >= maxPhasePerc) return 0;
	possible -= minPhasePerc;

	float p = current / (current + possible);
	if (p == 1) return 0;

	homogen = 1;
	float tauCur = tau*homogen;						//TODO make it right
	float dpdt = (n * (1-p) * (-log(1-p)) / tauCur) * pow (log(1-taup) / log(1-p), 1/n);

	float change = std::min(dpdt * dt, possible);
	return change;
}
