#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "phasestate.h"
#include <float.h>
#include <stdlib.h>
#include <vector>

//class that describes and calculate abstract transformation
class Transformation
{
private:
	std::vector <double> parameters;

protected:
	virtual double calcPhaseFraction (double T, double dt, double current, double possible) = 0;

public:
	Transformation (std::vector <double> parameters);
	~Transformation();

//	inline double getParm (unsigned int pid)	{ if (pid < parameters.capacity()) return parameters.at(pid); return DBL_MIN;}
	virtual phaseState step (double T, double dt, phaseState oldState, phaseState newState) = 0;
};

#endif // TRANSFORMATION_H
