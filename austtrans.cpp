#include "austtrans.h"
#include <cmath>
#include <algorithm>
#include "phasestate.h"

const float taup = 0.50; //tau - time at p = 50%
const float commonk = 0.25; //Ac1` Ac3` increase degree (common for everyone)

austTrans::austTrans()
{
	set(0, 0, 0, 0, 0);
}

austTrans::austTrans(float tau1, float p1, float tau2, float p2, float tauh, float ph, float Ac1, float Ac3)
{
	double n = calcn(tau1, p1, tau2, p2);
	double nh = calcn(tau1, p1, tauh, ph);
	float Tmin = calcTmin(Ac1, p1, Ac3, p2);
	float c = calcc(tau1, p1, n, Ac1, Tmin, commonk);
	set(c, n, nh, Tmin, commonk);
}

austTrans::austTrans(float tau1, float p1, double n, double nh, float Ac1, float Ac3, float pAc3)
{
	float Tmin = calcTmin(Ac1, p1, Ac3, pAc3);
	float c = calcc(tau1, p1, n, Ac1, Tmin, commonk);
	set(c, n, nh, Tmin, commonk);
}

//calculate c = exp(-b/k) (b is shift of incubation period at Tmin temperature)
float austTrans::calcc(float tau1, float p1, double n, float Ac1, float Tmin, float k)
{
	if (n <= 0 || p1 <= 0 || p1 >= 1 || tau1 <= 0) return -1;
	float tau;
	if (p1 == taup) tau = tau1;
	else tau = tau1*exp(log(log(1-taup)/log(1-p1))/n);

	return tau/pow(Ac1-Tmin, (k-1)/k);
}

//calculate avraami`s formula degree by 2 arbitraty incubation periods
double austTrans::calcn(float tau1, float p1, float tau2, float p2)
{//same as diffATrans
	if (p1 == p2 || p1 <= 0 || p1 >= 1 ||
					p2 <= 0 || p2 >= 1 ||
		tau1 == tau2 || tau1 <= 0 || tau2 <= 0)
		return -1;
	return log(log(1-p2)/log(1-p1))/log(tau2/tau1);
}

//calculate theoretical T min, where equilibrium austenite`s percentage is null
float austTrans::calcTmin(float Ac1, float pAc1, float Ac3, float pAc3)
{
	return (pAc3*Ac1 - pAc1*Ac3)/(pAc3-pAc1);
}

//calculate proper incubation period (1%) by temperature and phase percent
float austTrans::calctau(float T, float p)
{
	if (T <= Tmin || n <= 0 || p <= 0 || p >= 1 || c <= 0) return -1;

	float tau = pow(T - Tmin, (k-1)/k)*c;
	if (p == taup) return tau;

	return tau*exp(log(log(1-p)/log(1-taup))/n);
}

//calculate proper(1%) incubation period analogue for homogenization by temperature and phase percent
float austTrans::calctauh(float T, float p)
{
	if (T <= Tmin || nh <= 0 || p <= 0 || p >= 1 || c <= 0) return -1;

	float tau = pow(T - Tmin, (k-1)/k)*c;
	if (p == taup) return tau;

	return tau*exp(log(log(1-p)/log(1-taup))/nh);
}

//calculate shift of incubation period at Tmin temperature
float austTrans::getb()
{
	if (c <= 0 ||  k <= 0) return -1;
	return -k*log(c);
}

void austTrans::set(float c, double n, double nh,float Tmin, float k)
{
	this->c = c;
	this->n = n;
	this->nh = nh;
	this->Tmin = Tmin;
	this->k = k;
}

float austTrans::calcChange(float T, float dt, float current, float possible)
{
	if (T <= Tmin || n <= 0) return 0;

	if (possible <= minPhasePerc || current >= maxPhasePerc) return 0;
	possible -= minPhasePerc;

	float p = current / (current + possible);
	if (p == 1) return 0;

	float tau = calctau(T, taup);
	float dpdt = (n * pow(-log(1-taup), 1/n) / tau) * (1 - p) * pow (-log(1-p), (n-1)/n);

	float change = std::min(dpdt * dt, possible);
	return change;
}

float austTrans::calcHomogenization(float T, float dt, float current)
{
	if (T <= Tmin || nh <= 0) return 0;
	if (current >= 1) return 0;

	float p = current;
	float tau = calctauh(T, taup);
	float dpdt = (nh * pow(-log(1-taup), 1/nh) / tau) * (1 - p) * pow (-log(1-p), (nh-1)/nh);

	float change = std::min(dpdt * dt, 1.f-current);
	return change;
}
