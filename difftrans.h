#ifndef DIFFPROPS_H
#define DIFFPROPS_H

//class that describes and calculate diffusion transformation of Austenite
class diffTrans{
private:
	float tau;	//incubation period for 1% of phase
	double n;	//avraami`s degree, speed of transformation
	float Tmax;	//maximum temperature of phase`s transformation
	float Tmin;	//minimum temperature of phase`s transformation

	double calcn(float tau1, float p1, float tau2, float p2);
	float calctau(float tau1, float p1, double n);
	void set(float tau, double n, float Tmax, float Tmin);

public:
	diffTrans();
	diffTrans(float tau1, float p1, double n, float Tmax, float Tmin);
	diffTrans(float tau1, float p1, float tau2, float p2, float Tmax, float Tmin);

	inline float gettau()	{ return tau; }
	inline double getn()	{ return n; }
	inline float getTmax()	{ return Tmax; }
	inline float getTmin()	{ return Tmin; }

	float calctau(float p);

	float calcChange(float T, float dt, float current, float possible, float homogen);
};

#endif // DIFFPROPS_H
