#include "chemicalprops.h"
#include <cmath>
#include <algorithm>

float chemicalProps::ChemLimits[2][chemN] =
//                      C,	Si,	Mn,	Cr,	Mo,	Ni,	Cu,	V,	B,	W,	Ti,	Nb,	Zr,	Al,	Co,	S,	P,	N,	O,	Ce,	Y
///*Makarov min	*/{{	0.030,	0.030,	0.150,	0.000,	0.000,	0.000,	0.000,	0.000,	0.008,	0.000,	0.000,	0.000,	0.000,	0.010,	0.000,	0.002,	0.002,	0.001,	0.001,	0.001,	0.001	},
///*Makarov max	*/ {	0.450,	1.500,	2.000,	3.000,	1.000,	4.000,	1.200,	0.800,	0.008,	0.800,	0.800,	0.200,	0.200,	1.100,	0.500,	0.040,	0.040,	0.050,	0.010,	0.010,	0.010	}};
/*Rubtsovi min	*/{{	0.010,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000,	0.000	},
/*Rubtsovi max	*/ {	0.500,	1.000,	2.000,	2.000,	1.000,	2.000,	0.500,	0.300,	0.000,	0.500,	0.050,	0.100,	0.000,	0.100,	0.000,	0.030,	0.030,	0.000,	0.000,	0.000,	0.000	}};

//set chemical stucture and limit it by limits of regression model
chemicalProps::chemicalProps(UChem* chem)
{
	for (int i = 0; i < chemN; i++)
		this->chem.array[i] = chem->array[i];

	isChanged = false;
	const int MIN = 0, MAX = 1;
	for (int i = 0; i < chemN; i++)
	{
		if(this->chem.array[i] < ChemLimits[MIN][i])
		{
			this->chem.array[i] = ChemLimits[MIN][i];
			isChanged = true;
		}
		else if(this->chem.array[i] > ChemLimits[MAX][i])
		{
			this->chem.array[i] = ChemLimits[MAX][i];
			isChanged = true;
		}
	}
}

chemicalProps::~chemicalProps()
{
}

bool chemicalProps::changed()
{
	return isChanged;
}

UChem* chemicalProps::getChem()
{
	return &chem;
}


double chemicalProps::t85toTime(double t85, double Tmax, double Tmin)
{
    return t85 * log (Tmax/Tmin) / log (850./500.); //t85 is cooling time between 850°C and 500°C
}

//calculation of steel`s phases properties by regression formulae
Transformer* chemicalProps::regressProps()
{
        //Makarov
	//temperatures
	float Tfp5 = 862.141 - 81.749*chem.Mn - 55.316*chem.Cr - 97.518*chem.Mo - 11.117*chem.Ni
				- 185.28*chem.V - 367.375*chem.C*chem.C - 7.881*chem.Mn*chem.Mn + 7.763*chem.Cr*chem.Cr
				+ 2764.267*chem.V*chem.V + 141.801*chem.C*chem.Mn - 62.501*chem.Mn*chem.Si
				+ 72.565*chem.Mn*chem.Cr + 60.979*chem.Mn*chem.Mo - 21.627*chem.Mn*chem.Ni
				- 88.157*chem.Mn*chem.V - 67.722*chem.Si*chem.Cr - 852.215*chem.Si*chem.V
				- 494.502*chem.Mo*chem.V;
	float Tfp95 = 620 + 479.23*chem.C - 80.49*chem.Mn - 382*chem.V - 495.5*chem.C*chem.C
				+ 19.36*chem.Mn*chem.Mn - 12*chem.Cr*chem.Cr - 54.63*chem.Mo*chem.Mo + 1574.4*chem.V*chem.V
				- 54.11*chem.C*chem.Ni - 57.94*chem.Mn*chem.Cr + 36.12*chem.Cr*chem.Mo
                                + 125.14*chem.Cr*chem.V - 641.55*chem.Mo*chem.V;
	float Tm5 = 562 - 660*chem.C - 23.3*chem.Mn - 35*chem.Cr - 10*chem.Mo - 17.5*chem.Ni - 9.4*chem.Cu
				- 3*chem.V + 291*chem.C*chem.C + 2.5*chem.Cr*chem.Cr - 0.17*chem.Mo*chem.Mo;
	float Tm90 = Tm5 - (120 + 270*chem.C);
	float Tb5 = Tfp95;
	float Tb90 = Tm90 - (Tm5 - 350);
	//cooling speeds w6/5
	float Ceq = chem.C + chem.Si/24 + chem.Mn/6 + chem.Cr/5 + chem.Mo/4 + chem.Ni/10 + chem.Cu/15
				+ chem.V/14 + 5*chem.B;
	float wm90 = 3.217*pow(Ceq, -3.838);
	float wb90 = 0.343*pow(Ceq, -4.116);
	float wfp95 = 0.1 * wb90;
	//coefficients
        float nfpMakarov = 4.067/log(wb90/wfp95);
        float nbMakarov = 3.3475/log(wm90/wb90);
        float kfp = 2.9957/pow(wb90, nfpMakarov);
        float kb = 0.1054/pow(wb90, nbMakarov);

        //Kasatkin
        //Austenite
        float Ac1 = 723 -7.08*chem.Mn +37.7*chem.Si +18.1*chem.Cr +44.2*chem.Mo -8.95*chem.Ni -50.1*chem.V
				+(-11.5*chem.Si -59.7*chem.Mo -5.28*chem.Ni -27.4*chem.V)*chem.C +21.7*chem.Al
				+3.18*chem.W +297.*chem.S -830.*chem.N -14.*chem.Mn*chem.Si -15.5*chem.Mn*chem.Mo
                                -6.*chem.Mn*chem.Ni -3.1*chem.Si*chem.Cr +6.77*chem.Si*chem.Ni -.8*chem.Cr*chem.Ni
				-30.8*chem.Mo*chem.V -0.84*chem.Cr*chem.Cr -3.46*chem.Mo*chem.Mo -0.46*chem.Ni*chem.Ni
                                -28.0*chem.V*chem.V;
        float Ac3 = 912.-370.*chem.C -27.4*chem.Mn +27.3*chem.Si -6.35*chem.Cr -32.7*Ni +95.2*chem.V +70.2*chem.Ti
                                +72.*chem.Al +64.5*Nb +5.57*chem.W  +332.*chem.S +276.*chem.P -485.*chem.N -900.*chem.B
                                +(16.2*chem.Mn +32.*chem.Si +15.4*chem.Cr +48.*Ni)*chem.C +4.8*chem.Mn*Ni
                                +4.32*chem.Si*chem.Cr -17.3*chem.Si*chem.Mo -18.6*chem.Si*Ni +40.5*chem.Mo*chem.V
                                +174.*chem.C*chem.C +2.46*chem.Mn*chem.Mn -6.86*chem.Si*chem.Si +0.32*chem.Cr*chem.Cr
                                +9.9*chem.Mo*chem.Mo +1.24*Ni*Ni -60.2*chem.V*chem.V;
        //??? Dardel, Baiter
        //Liquid
        float liquidus = 1537 -88*chem.C -8*chem.Si -5*chem.Mn -1.5*chem.Cr -4*chem.Ni -2*chem.Mo
                                        -2*chem.V -5*chem.Cu -18*chem.Ti -25*chem.S -30*chem.P;
        float solidus = 1535 -200.*chem.C -12.3*chem.Si -6.8*chem.Mn -1.4*chem.Cr -4.3*chem.Ni
                                        -4.1*chem.Al +183.9*chem.S -124.5*chem.P;

        //Rubtsovi
        double TfpMax = 895 -714*chem.C -77*chem.Mn +28*chem.Cr -60*chem.Ni -434*chem.Al +687*chem.C*chem.C +91*chem.C*chem.Mn;
        double nfp = 1.1 -3.6*chem.C +0.55*chem.Mn +0.6*chem.Cr +0.3*chem.Ni -1.95*chem.Cu -0.6*chem.Al +4.3*chem.C*chem.C -0.4*chem.Mn*chem.Mn +2.9*chem.C*chem.Mn;
        double lnTau50fp = 0.72 +1.1*chem.Si +1.1*chem.Mn +3.3*chem.Cr +3.5*chem.Ni +3.7*chem.Mo +7*chem.Cu +5.3*chem.Al -5.2*chem.Ti +9.7*chem.C*chem.Mn;

        double TbMax = 700 -237*chem.C -20*chem.Si -26*chem.Mn -23*chem.Cr -24*chem.Ni -89*chem.V -28*chem.Cu -227*chem.Al -715*chem.Nb;
        double nb = 4 -1.4*chem.C +0.2*chem.Si -1.2*chem.Mn +1.6*chem.V -2.3*chem.Cu;
        double lnTau50b = -1.2 + 6.2*chem.C +0.7*chem.Si +1.25*chem.Mn +0.3*chem.Ni +1.4*chem.Mo +0.9*chem.V +2*chem.Cu;

        double Tms = 534 -430*chem.C -33*chem.Mn -9*chem.Cr -21*chem.Ni -11*chem.Mo -39*chem.V +91*chem.C*chem.C;
        double TmMax = Tms -25; //not ready yet
        double bm = -0.011; //not ready yet

        double t50fp = t85toTime(exp(lnTau50fp), TfpMax, TbMax);
        double t50b = t85toTime(exp(lnTau50b), TbMax, Tms);

        //HV temporary here
        double HVfp = 105 +310*chem.C +16*chem.Mn -140*chem.Mo;
        double HVb = 195 +136*chem.C +29*chem.Si +35*chem.Cr +29*chem.Ni +132*chem.V +16*chem.W +173*chem.Al;
        double HVm = 289 +792*chem.C +37*chem.Si +15*chem.W;

	//constant calculated % of new phase
        const float p50 = 0.50;
	const float pfp5 = 0.05;
	const float pb5 = 0.05;
	const float pm5 = 0.05;
	const float pfp95 = 0.95;
	const float pb90 = 0.90;
	const float pm90 = 0.90;
	const float solp = 0.05;
	const float liqp = 0.95;

	const float c = log(((float)600)/500)/100; //w6/5 constant
	//regress end

	//fp + b
	float Taufpt = log(Tfp5/Tfp95)/(c*wfp95);	//time of transformation 0.05 - 0.9*
	float Taubt  = log(Tb5 /Tb90 )/(c*wb90 );

        float Taufp5 = Taufpt/(exp(log(log(1-pfp95)/log(1-pfp5))/nfpMakarov)-1);	//incubation period (up to 5%)
        float Taub5  = Taubt /(exp(log(log(1-pb90 )/log(1-pb5 ))/nbMakarov )-1);

	float Tfps = Tfp5/exp(-c*wfp95*Taufp5);		//max temperature of transformation
	float Tbs  = Tb5 /exp(-c*wb90 *Taub5 );

	float wfp05 = (wfp95 + wb90)/2;		//average w6/5
	float wb05  = (wb90  + wm90)/2;

        float pfp05 = exp(-kfp*pow(wfp05,nfpMakarov));		//result % of new phase for average speed
        float pb05  = exp(-kb *pow(wb05 ,nbMakarov ));

        float Taufpt05 = Taufp5*exp(log(log(1-pfp05)/log(1-pfp5))/nfpMakarov)-1;	//time of transformation 0.5 - p at average speed
        float Taubt05  = Taub5 *exp(log(log(1-pb05 )/log(1-pb5 ))/nbMakarov )-1;

	float Tfpe = (Tfps/exp(c*wfp05*Taufp5))*exp(-c*wfp05*Taufpt05);	//min temperature of transformation
	float Tbe  = (Tbs /exp(c*wb05 *Taub5 ))*exp(-c*wb05 *Taubt05 );

	nodiffTrans* Liquid = new nodiffTrans(solidus, solp, liquidus, liqp);
        austTrans* Austenite = new austTrans();								///TODO: Fill
        diffTrans* Ferrite = new diffTrans(t50fp, p50, nfp, TfpMax, TbMax);	// united Ferrite and Perlite
        diffTrans* Bainite = new diffTrans(t50b, p50, nb, TbMax, TmMax);
        nodiffTrans* Martensite = new nodiffTrans(bm, TmMax);

	return new Transformer(Liquid, Austenite, Ferrite, Bainite, Martensite, Ac1, Ac3);
}
