#ifndef AUSTENITETRANSFORMATION_H
#define AUSTENITETRANSFORMATION_H

#include "avramitransformation.h"

class AusteniteTransformation : public AvramiTransformation
{
public:
	AusteniteTransformation (std::vector <double> parameters);
	~AusteniteTransformation ();

	phaseState step (double T, double dt, phaseState oldState, phaseState newState);
	double halfLifeTime (double T);
};

#endif // AUSTENITETRANSFORMATION_H
