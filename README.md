# SSTDiag

SSTDiag is a tool for modelling Solid State Transformation based on  a simplified model with a minimal amount of parameters and drawing CCT, TTT and dilatogram.

## Contributing.
Development is discontinued.

## License

[MIT](https://choosealicense.com/licenses/mit/)
