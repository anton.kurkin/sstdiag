#ifndef MATTRANSPROPS_H
#define MATTRANSPROPS_H

#include "difftrans.h"
#include "austtrans.h"
#include "nodifftrans.h"
#include "phasestate.h"
#include "cyclestate.h"
#include <QVector>
#include <QPair>

//class that contains steel`s phases properties and calculates transformation steps
class Transformer
{
private:
	nodiffTrans *Liquid;
	austTrans *Austenite;

	diffTrans *Ferrite;
	diffTrans *Bainite;
	nodiffTrans *Martensite;

	float Ac1, Ac3;

public:
	Transformer(nodiffTrans *Liquid, austTrans *Austenite, diffTrans *Ferrite, diffTrans *Bainite, nodiffTrans *Martensite, float Ac1, float Ac3);
	~Transformer();

	phaseState* calcState(phaseState* current, float T, float dt);
	QVector<cycleState*>* calcCycle(QVector<QPair<float, float>* >* termalCycle, float TStart, float deltat, UPhases* stateStart);
	QVector<QVector<cycleState*>*>* calcTKD(float wstart, float wend, float pw, bool w65, float Tstart, float Tend, float deltaT, UPhases* phasesStart);
	inline nodiffTrans*		getLiquid()				{ return Liquid; }
	inline austTrans*		getAustenite()			{ return Austenite; }
	inline diffTrans*		getFerrite()			{ return Ferrite; }
	inline diffTrans*		getBainite()			{ return Bainite; }
	inline nodiffTrans*		getMartensite()			{ return Martensite; }
	inline float			getAc1()				{ return Ac1; }
	inline float			getAc3()				{ return Ac3; }
};
#endif // MATTRANSPROPS_H
