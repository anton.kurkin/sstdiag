#ifndef CHEMICALPROPS_H
#define CHEMICALPROPS_H

#include "transformer.h"

enum chemEnum
{
	C,	Si,	Mn,	Cr,	Mo,	Ni,	Cu,	V,	B,	W,	Ti,	Nb,	Zr,	Al,	Co,	S,	P,	N,	O,	Ce,	Y, chemN
};

union UChem
{
	struct
	{
		float	C,	Si,	Mn,	Cr,	Mo,	Ni,	Cu,	V,	B,	W,	Ti,	Nb,	Zr,	Al,	Co,	S,	P,	N,	O,	Ce,	Y;
	};
	float array[chemN];
};

//class that regression based on chemical structure
class chemicalProps
{
public:
	chemicalProps(UChem* chem);
	~chemicalProps();

	bool changed();
	UChem* getChem();

	Transformer* regressProps();

private:
	UChem chem;
	bool isChanged; //was structure changed by limits of regression model?
        double t85toTime(double t85, double Tmax, double Tmin);

public:
	static float ChemLimits[2][chemN];
};

#endif // CHEMICALPROPS_H
