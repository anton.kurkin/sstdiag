#-------------------------------------------------
#
# Project created by QtCreator 2011-10-19T15:02:39
#
#-------------------------------------------------

QT       += core gui
QT       += svg

TARGET = SSTdiag
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    displaywidget.cpp \
    chemicalprops.cpp \
    transformation.cpp \
    nodifftrans.cpp \
    cyclestate.cpp \
    phasestateanalizer.cpp \
    austtrans.cpp \
    difftrans.cpp \
    avramitransformation.cpp \
    transformer.cpp \
    phasestate.cpp \
    austenitetransformation.cpp

HEADERS  += mainwindow.h \
    displaywidget.h \
    chemicalprops.h \
    transformation.h \
    nodifftrans.h \
    cyclestate.h \
    phasestateanalizer.h \
    austtrans.h \
    difftrans.h \
    avramitransformation.h \
    transformer.h \
    phasestate.h \
    austenitetransformation.h

FORMS    += mainwindow.ui




